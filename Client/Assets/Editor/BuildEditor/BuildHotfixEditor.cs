using System.IO;
using UnityEditor;
using UnityEngine;

namespace MHEditor
{
    [InitializeOnLoad]
    public class Startup
    {
        private const string ScriptAssembliesDir = "Library/ScriptAssemblies";
        private const string CodeDir = "Assets/Res/";
        private const string HotfixDll = "Hotfix.dll";
        private const string HotfixPdb = "Hotfix.pdb";

        static Startup()
        {
            File.Copy(Path.Combine(ScriptAssembliesDir, HotfixDll), Path.Combine(CodeDir, "Hotfix.dll.bytes"), true);
            File.Copy(Path.Combine(ScriptAssembliesDir, HotfixPdb), Path.Combine(CodeDir, "Hotfix.pdb.bytes"), true);
            Debug.Log($"复制Hotfix.dll, Hotfix.pdb到Res完成");
            AssetDatabase.Refresh();
        }
    }
}