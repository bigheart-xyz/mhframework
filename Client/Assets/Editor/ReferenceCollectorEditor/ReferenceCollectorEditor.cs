using System;
using System.Collections.Generic;
using Model;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

[CustomEditor(typeof (ReferenceCollector))]
[CanEditMultipleObjects]
public class ReferenceCollectorEditor: Editor
{
	private static readonly string[] classNames = new[]
    {
        "Object",
        "GameObject",
        "Transform",
        "CanvasGroup",
        "Graphic",
        "Text",
        "TextMeshProUGUI",
        "Image",
        "RawImage",
        "Slider",
        "Scrollbar",
        "Dropdown",
        "Toggle",
        "ToggleGroup",
        "Button",
        "InputField",
        "ScrollRect",
        "HorizontalLayoutGroup",
        "VerticalLayoutGroup",
        "GridLayoutGroup",
    };

    private static readonly Dictionary<string, int> classIndexes = new Dictionary<string, int>()
    {
        {"Object", 0},
        {"GameObject", 1},
        {"Transform", 2},
        {"CanvasGroup", 3},
        {"Graphic", 4}, 
        {"Text", 5},
        {"TextMeshProUGUI", 6},
        {"Image", 7},
        {"RawImage", 8},
        {"Slider", 9},
        {"Scrollbar", 10},
        {"Dropdown", 11},
        {"Toggle", 12},
        {"ToggleGroup", 13},
        {"Button", 14},
        {"InputField", 15},
        {"ScrollRect", 16},
        {"HorizontalLayoutGroup", 17},
        {"VerticalLayoutGroup", 18},
        {"GridLayoutGroup", 19},
    };

    private static readonly Dictionary<string, Type> classTypes = new Dictionary<string, Type>()
    {
        {"Object", typeof(Object)},
        {"GameObject", typeof(GameObject)},
        {"Transform", typeof(Transform)},
        {"CanvasGroup", typeof(CanvasGroup)},
        {"Graphic", typeof(Graphic)},
        {"Text", typeof(Text)},
        {"TextMeshProUGUI", typeof(TextMeshProUGUI)},
        {"Image", typeof(Image)},
        {"RawImage", typeof(RawImage)},
        {"Slider", typeof(Slider)},
        {"Scrollbar", typeof(Scrollbar)},
        {"Dropdown", typeof(Dropdown)},
        {"Toggle", typeof(Toggle)},
        {"ToggleGroup", typeof(ToggleGroup)},
        {"Button", typeof(Button)},
        {"InputField", typeof(InputField)},
        {"ScrollRect", typeof(ScrollRect)},
        {"HorizontalLayoutGroup", typeof(HorizontalLayoutGroup)},
        {"VerticalLayoutGroup", typeof(VerticalLayoutGroup)},
        {"GridLayoutGroup", typeof(GridLayoutGroup)},
    };
    
    private string searchKey
	{
		get => _searchKey;
		set
		{
			if (_searchKey != value)
			{
				_searchKey = value;
				heroPrefab = referenceCollector.Get<Object>(searchKey);
			}
		}
	}

	private ReferenceCollector referenceCollector;

	private Object heroPrefab;

	private string _searchKey = "";

	private void DelNullReference()
	{
		var dataProperty = serializedObject.FindProperty("data");
		for (int i = dataProperty.arraySize - 1; i >= 0; i--)
		{
			var subProperty = dataProperty.GetArrayElementAtIndex(i).FindPropertyRelative("data");
			if (subProperty.objectReferenceValue == null)
			{
				dataProperty.DeleteArrayElementAtIndex(i);
			}
		}
	}

	private void OnEnable()
	{
		referenceCollector = (ReferenceCollector) target;
	}

	public override void OnInspectorGUI()
	{
		Undo.RecordObject(referenceCollector, "Changed Settings");
		var dataProperty = serializedObject.FindProperty("data");
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("添加引用"))
		{
			AddReference(dataProperty, Guid.NewGuid().GetHashCode().ToString(), null);
		}
		if (GUILayout.Button("全部删除"))
		{
			dataProperty.ClearArray();
		}
		if (GUILayout.Button("删除空引用"))
		{
			DelNullReference();
		}
		if (GUILayout.Button("排序"))
		{
			referenceCollector.Sort();
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		searchKey = EditorGUILayout.TextField(searchKey);
		EditorGUILayout.ObjectField(heroPrefab, typeof (Object), false);
		if (GUILayout.Button("删除"))
		{
			referenceCollector.Remove(searchKey);
			heroPrefab = null;
		}
		GUILayout.EndHorizontal();
		EditorGUILayout.Space();

		var delList = new List<int>();
		for (int i = referenceCollector.data.Count - 1; i >= 0; i--)
		{
			GUILayout.BeginHorizontal();
			string newKey = EditorGUILayout.TextField(referenceCollector.data[i].key, GUILayout.Width(150));
			string type = referenceCollector.data[i].type ?? classNames[0];
			classIndexes.TryGetValue(type, out int index);
			int newIndex = EditorGUILayout.Popup(index, classNames);
			string newType = classNames[newIndex];
			var newT = classTypes[newType];
			var obj = referenceCollector.data[i].data;
			var newObj = obj;
			if (index <= 1)
			{
				var go = obj as GameObject;
				if (go != null)
				{
					if (newIndex >= 2) newObj = go.GetComponent(newT);
				}
			}
			else
			{
				var c = obj as UnityEngine.Component;
				if (c != null)
				{
					if (newIndex == 1) newObj = c.gameObject;
					else if (newIndex >= 2) newObj = c.gameObject.GetComponent(newT);
				}
				else
				{
					var go = obj as GameObject;
					if (go != null)
					{
						if (newIndex == 1) newObj = go;
						else if (newIndex >= 2) newObj = go.GetComponent(newT);
					}
				}
			}

			newObj = EditorGUILayout.ObjectField(newObj, classTypes[newType], true);
			if (GUILayout.Button("X"))
			{
				delList.Add(i);
			}
			else if (newKey != referenceCollector.data[i].key || newIndex != index ||
			         newObj != referenceCollector.data[i].data)
			{
				var element = dataProperty.GetArrayElementAtIndex(i);
				element.FindPropertyRelative("key").stringValue = newKey;
				element.FindPropertyRelative("type").stringValue = newType;
				if (newObj != null) element.FindPropertyRelative("data").objectReferenceValue = newObj;
			}

			GUILayout.EndHorizontal();
		}
        
		var eventType = Event.current.type;
		if (eventType == EventType.DragUpdated || eventType == EventType.DragPerform)
		{
			// Show a copy icon on the drag
			DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

			if (eventType == EventType.DragPerform)
			{
				DragAndDrop.AcceptDrag();
				foreach (var o in DragAndDrop.objectReferences)
				{
					AddReference(dataProperty, o.name, o);
				}
			}

			Event.current.Use();
		}
		
		foreach (var i in delList)
		{
			dataProperty.DeleteArrayElementAtIndex(i);
		}
		
		serializedObject.ApplyModifiedProperties();
		serializedObject.UpdateIfRequiredOrScript();
	}
	
	private void AddReference(SerializedProperty dataProperty, string key, Object obj)
	{
		int index = dataProperty.arraySize;
		dataProperty.InsertArrayElementAtIndex(index);
		var element = dataProperty.GetArrayElementAtIndex(index);
		string type = classNames[0];
		var go = obj as GameObject;
		if (go != null)
		{
			int i;
			for (i = classNames.Length - 1; i > 2; i--)
			{
				string tempType = classNames[i];
				var tempT = classTypes[tempType];
				var c = go.GetComponent(tempT);
				if (c != null)
				{
					type = tempType;
					obj = c;
					break;
				}
			}

			if (i == 2) type = classNames[1];
		}

		element.FindPropertyRelative("key").stringValue = key;
		element.FindPropertyRelative("type").stringValue = type;
		element.FindPropertyRelative("data").objectReferenceValue = obj;
	}
}
