using System.IO;
using UnityEditor.AssetImporters;
using UnityEditor.Experimental.AssetImporters;
using UnityEngine;

[ScriptedImporter(1, "bin")]
public class BinImporter : ScriptedImporter
{
    public override void OnImportAsset(AssetImportContext ctx)
    {
        var bytes = File.ReadAllBytes(ctx.assetPath);
        var asset = ScriptableObject.CreateInstance<ByteAsset>();
        asset.bytes = bytes;
        ctx.AddObjectToAsset("main obj", asset);
        ctx.SetMainObject(asset);
    }
}