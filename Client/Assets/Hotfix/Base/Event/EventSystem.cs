﻿using System;
using System.Collections.Generic;
using Model;
using UnityEngine;

namespace Hotfix
{
    public static class EventSystem
    {
	    private static readonly Dictionary<long, Component> allComponents = new Dictionary<long, Component>();
	    
        private static readonly List<Type> types = new List<Type>();
        
        private static readonly Dictionary<string, Type> allUIComponents = new Dictionary<string, Type>();
        
        private static readonly Dictionary<string, List<IEvent>> allEvents = new Dictionary<string, List<IEvent>>();
        
        private static readonly UnOrderMultiMap<Type, IAwakeSystem> awakeSystems = new UnOrderMultiMap<Type, IAwakeSystem>();

        private static readonly UnOrderMultiMap<Type, IStartSystem> startSystems = new UnOrderMultiMap<Type, IStartSystem>();

        private static readonly UnOrderMultiMap<Type, IDestroySystem> destroySystems = new UnOrderMultiMap<Type, IDestroySystem>();

        private static readonly UnOrderMultiMap<Type, ILoadSystem> loadSystems = new UnOrderMultiMap<Type, ILoadSystem>();

        private static readonly UnOrderMultiMap<Type, IUpdateSystem> updateSystems = new UnOrderMultiMap<Type, IUpdateSystem>();

        private static readonly UnOrderMultiMap<Type, ILateUpdateSystem> lateUpdateSystems = new UnOrderMultiMap<Type, ILateUpdateSystem>();

        private static readonly UnOrderMultiMap<Type, IDeserializeSystem> deserializeSystems = new UnOrderMultiMap<Type, IDeserializeSystem>();

        private static Queue<long> updates = new Queue<long>();
        private static Queue<long> updates2 = new Queue<long>();

        private static readonly Queue<long> starts = new Queue<long>();

        private static Queue<long> loaders = new Queue<long>();
        private static Queue<long> loaders2 = new Queue<long>();

        private static Queue<long> lateUpdates = new Queue<long>();
        private static Queue<long> lateUpdates2 = new Queue<long>();
        
        public static void Init()
        {
            types.Clear();
			
			List<Type> ts = Model.HotfixManager.GetHotfixTypes();
			
			foreach (Type type in ts)
			{
				// ILRuntime无法判断是否有Attribute
				//if (type.GetCustomAttributes(typeof (Attribute), false).Length == 0)
				//{
				//	continue;
				//}
				
				types.Add(type);
			}

			foreach (Type type in types)
			{
				object[] attrs = type.GetCustomAttributes(typeof(ObjectSystemAttribute), false);

				if (attrs.Length == 0)
				{
					continue;
				}

				object obj = Activator.CreateInstance(type);

				switch (obj)
				{
					case IAwakeSystem objectSystem:
						awakeSystems.Add(objectSystem.Type(), objectSystem);
						break;
					case IUpdateSystem updateSystem:
						updateSystems.Add(updateSystem.Type(), updateSystem);
						break;
					case ILateUpdateSystem lateUpdateSystem:
						lateUpdateSystems.Add(lateUpdateSystem.Type(), lateUpdateSystem);
						break;
					case IStartSystem startSystem:
						startSystems.Add(startSystem.Type(), startSystem);
						break;
					case IDestroySystem destroySystem:
						destroySystems.Add(destroySystem.Type(), destroySystem);
						break;
					case ILoadSystem loadSystem:
						loadSystems.Add(loadSystem.Type(), loadSystem);
						break;
					case IDeserializeSystem deserializeSystem:
						deserializeSystems.Add(deserializeSystem.Type(), deserializeSystem);
						break;
				}
			}
			
			allEvents.Clear();
			foreach (Type type in types)
			{
				object[] attrs = type.GetCustomAttributes(typeof(EventAttribute), false);

				foreach (object attr in attrs)
				{
					EventAttribute aEventAttribute = (EventAttribute)attr;
					object obj = Activator.CreateInstance(type);
					IEvent iEvent = obj as IEvent;
					if (iEvent == null)
					{
						Debug.LogError($"{obj.GetType().Name} 没有继承IEvent");
					}
					RegisterEvent(aEventAttribute.Type, iEvent);

					// hotfix的事件也要注册到mono层，hotfix可以订阅mono层的事件
					Action<List<object>> action = list => { Handle(iEvent, list); };
					Model.EventSystem.RegisterEvent(aEventAttribute.Type, new EventProxy(action));
				}
			}
			
			allUIComponents.Clear();
			foreach (Type type in types)
			{
				object[] attrs = type.GetCustomAttributes(typeof(UIAttribute), false);

				foreach (object attr in attrs)
				{
					UIAttribute aUIAttribute = (UIAttribute)attr;
					if (allUIComponents.ContainsKey(aUIAttribute.Name))
					{
						Debug.LogError($"{aUIAttribute.Name} 有重复的Component");
						continue;
					}
					allUIComponents.Add(aUIAttribute.Name, type);
				}
			}
			
			Load();
        }

        public static Type GetUICompoent(string name)
        {
	        return allUIComponents[name];
        }
        
        public static void Handle(IEvent iEvent, List<object> param)
        {
	        switch (param.Count)
	        {
		        case 0:
			        iEvent.Handle();
			        break;
		        case 1:
			        iEvent.Handle(param[0]);
			        break;
		        case 2:
			        iEvent.Handle(param[0], param[1]);
			        break;
		        case 3:
			        iEvent.Handle(param[0], param[1], param[2]);
			        break;
	        }
        }
        
        public static void RegisterEvent(string eventId, IEvent e)
        {
	        if (!allEvents.ContainsKey(eventId))
	        {
		        allEvents.Add(eventId, new List<IEvent>());
	        }
	        allEvents[eventId].Add(e);
        }
        
        public static List<Type> GetTypes()
        {
	        return types;
        }
        
        public static void Add(Component component)
        {
	        allComponents.Add(component.InstanceId, component);

	        Type type = component.GetType();

	        if (loadSystems.ContainsKey(type))
	        {
		        loaders.Enqueue(component.InstanceId);
	        }

	        if (updateSystems.ContainsKey(type))
	        {
		        updates.Enqueue(component.InstanceId);
	        }

	        if (startSystems.ContainsKey(type))
	        {
		        starts.Enqueue(component.InstanceId);
	        }

	        if (lateUpdateSystems.ContainsKey(type))
	        {
		        lateUpdates.Enqueue(component.InstanceId);
	        }
        }
        
        public static void Remove(long instanceId)
        {
	        allComponents.Remove(instanceId);
        }
        
        public static void Deserialize(Component component)
        {
	        List<IDeserializeSystem> iDeserializeSystems = deserializeSystems[component.GetType()];
	        if (iDeserializeSystems == null)
	        {
		        return;
	        }

	        foreach (IDeserializeSystem deserializeSystem in iDeserializeSystems)
	        {
		        if (deserializeSystem == null)
		        {
			        continue;
		        }

		        try
		        {
			        deserializeSystem.Run(component);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }
        
        public static void Awake(Component component)
		{
			List<IAwakeSystem> iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}

			foreach (IAwakeSystem aAwakeSystem in iAwakeSystems)
			{
				if (aAwakeSystem == null)
				{
					continue;
				}
				
				IAwake iAwake = aAwakeSystem as IAwake;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}

		public static void Awake<P1>(Component component, P1 p1)
		{
			List<IAwakeSystem> iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}
			
			foreach (IAwakeSystem iAwakeSystem in iAwakeSystems)
			{
				if (iAwakeSystem == null)
				{
					continue;
				}
				
				IAwake<P1> iAwake = iAwakeSystem as IAwake<P1>;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component, p1);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}

		public static void Awake<P1, P2>(Component component, P1 p1, P2 p2)
		{
			List<IAwakeSystem> iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}

			foreach (IAwakeSystem iAwakeSystem in iAwakeSystems)
			{
				if (iAwakeSystem == null)
				{
					continue;
				}
				
				IAwake<P1, P2> iAwake = iAwakeSystem as IAwake<P1, P2>;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component, p1, p2);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}

		public static void Awake<P1, P2, P3>(Component component, P1 p1, P2 p2, P3 p3)
		{
			List<IAwakeSystem> iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}

			foreach (IAwakeSystem iAwakeSystem in iAwakeSystems)
			{
				if (iAwakeSystem == null)
				{
					continue;
				}
				
				IAwake<P1, P2, P3> iAwake = iAwakeSystem as IAwake<P1, P2, P3>;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component, p1, p2, p3);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}
        
		public static void Load()
		{
			while (loaders.Count > 0)
			{
				long instanceId = loaders.Dequeue();
				Component component;
				if (!allComponents.TryGetValue(instanceId, out component))
				{
					continue;
				}
				if (component.IsDisposed)
				{
					continue;
				}
				
				List<ILoadSystem> iLoadSystems = loadSystems[component.GetType()];
				if (iLoadSystems == null)
				{
					continue;
				}

				loaders2.Enqueue(instanceId);

				foreach (ILoadSystem iLoadSystem in iLoadSystems)
				{
					try
					{
						iLoadSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}

			ObjectHelper.Swap(ref loaders, ref loaders2);
		}
		
		private static void Start()
		{
			while (starts.Count > 0)
			{
				long instanceId = starts.Dequeue();
				Component component;
				if (!allComponents.TryGetValue(instanceId, out component))
				{
					continue;
				}

				List<IStartSystem> iStartSystems = startSystems[component.GetType()];
				if (iStartSystems == null)
				{
					continue;
				}

				foreach (IStartSystem iStartSystem in iStartSystems)
				{
					try
					{
						iStartSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}
		}
		
		public static void Destroy(Component component)
		{
			List<IDestroySystem> iDestroySystems = destroySystems[component.GetType()];
			if (iDestroySystems == null)
			{
				return;
			}

			foreach (IDestroySystem iDestroySystem in iDestroySystems)
			{
				if (iDestroySystem == null)
				{
					continue;
				}

				try
				{
					iDestroySystem.Run(component);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}

		public static void Update()
		{
			Start();
			
			while (updates.Count > 0)
			{
				long instanceId = updates.Dequeue();
				Component component;
				if (!allComponents.TryGetValue(instanceId, out component))
				{
					continue;
				}
				if (component.IsDisposed)
				{
					continue;
				}
				
				List<IUpdateSystem> iUpdateSystems = updateSystems[component.GetType()];
				if (iUpdateSystems == null)
				{
					continue;
				}

				updates2.Enqueue(instanceId);

				foreach (IUpdateSystem iUpdateSystem in iUpdateSystems)
				{
					try
					{
						iUpdateSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}

			ObjectHelper.Swap(ref updates, ref updates2);
		}

		public static void LateUpdate()
		{
			while (lateUpdates.Count > 0)
			{
				long instanceId = lateUpdates.Dequeue();
				Component component;
				if (!allComponents.TryGetValue(instanceId, out component))
				{
					continue;
				}
				if (component.IsDisposed)
				{
					continue;
				}
				
				List<ILateUpdateSystem> iLateUpdateSystems = lateUpdateSystems[component.GetType()];
				if (iLateUpdateSystems == null)
				{
					continue;
				}

				lateUpdates2.Enqueue(instanceId);

				foreach (ILateUpdateSystem iLateUpdateSystem in iLateUpdateSystems)
				{
					try
					{
						iLateUpdateSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}

			ObjectHelper.Swap(ref lateUpdates, ref lateUpdates2);
		}
		
        public static void Run(string type)
        {
	        List<IEvent> iEvents;
	        if (!allEvents.TryGetValue(type, out iEvents))
	        {
		        return;
	        }
	        foreach (IEvent iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle();
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }

        public static void Run<A>(string type, A a)
        {
	        List<IEvent> iEvents;
	        if (!allEvents.TryGetValue(type, out iEvents))
	        {
		        return;
	        }
	        foreach (IEvent iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle(a);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }

        public static void Run<A, B>(string type, A a, B b)
        {
	        List<IEvent> iEvents;
	        if (!allEvents.TryGetValue(type, out iEvents))
	        {
		        return;
	        }
	        foreach (IEvent iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle(a, b);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }

        public static void Run<A, B, C>(string type, A a, B b, C c)
        {
	        List<IEvent> iEvents;
	        if (!allEvents.TryGetValue(type, out iEvents))
	        {
		        return;
	        }
	        foreach (IEvent iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle(a, b, c);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }
    }
}
