﻿using System.Collections.Generic;

namespace Hotfix
{
    public class ListStack<T>
    {
        private readonly List<T> list = new List<T>();
        private int index;

        public int Count => list.Count;

        public T this[int i] => list[i];

        public void Push(T item)
        {
            list.Add(item);
            index++;
        }

        public T Pop()
        {
            if (index < 0) return default(T);

            var item = list[index];
            index--;
            return item;
        }

        public T Peek()
        {
            if (index < 0) return default(T);

            return list[index];
        }

        public void Clear()
        {
            list.Clear();
            index = 0;
        }

    }
}
