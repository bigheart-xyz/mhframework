﻿using System;

namespace Hotfix
{
	public static class Log
	{
		public static void Info(string msg)
		{
			Model.Log.Info(msg);
		}
		
		public static void Warning(string msg)
		{
			Model.Log.Warning(msg);
		}

		public static void Error(string msg)
		{
			Model.Log.Error(msg);
		}

		public static void Error(Exception e)
		{
			Model.Log.Error(e.ToStr());
		}
		
		public static void Fatal(string msg)
		{
			Model.Log.Fatal(msg);
		}
		
		
		public static void Msg(object msg)
		{
			Info(Dumper.DumpAsString(msg));
		}
	}
}