﻿using System.Threading.Tasks;

namespace Hotfix
{
	public interface IDisposable
	{
		void Dispose();
	}

	public interface ISupportInitialize
	{
		Task BeginInit();
		Task EndInit();
	}

	public abstract class Object: ISupportInitialize
	{
		public virtual async Task BeginInit()
		{
			await Task.CompletedTask;
		}

		public virtual async Task EndInit()
		{
			await Task.CompletedTask;
		}

		public override string ToString()
		{
			return JsonHelper.ToJson(this);
		}
	}
}