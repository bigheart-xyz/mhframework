﻿using Model;
using UnityEngine;

namespace Hotfix
{
	[Event(EventIdType.InitSceneStart)]
	public class InitSceneStart: AEvent
	{
		public override async void Run()
		{
			var gui = await ResourceManager.LoadUIPrefabAsync(Define.GUI);
			gui.name = Define.GUI;
			UnityEngine.Object.DontDestroyOnLoad(gui);
			Init.GUI = gui;
			
			await PopUpMaskManager.Init();

			UIManager.FadeToPanel(UIName.LoginPanel);
			
			// Log.Info(ConfigManager.GetOne<LocalizationConfig>().Value_CH);
			Debug.Log(ConfigManager.Tables.TbBattleConst.BaseExp31);
		}
	}
}
