﻿using Bright.Serialization;
using Model;

namespace Hotfix
{
    public static class ConfigManager
    {
        public static Tables Tables { get; private set; }
        
        public static void Init()
        {
            Tables = new Tables(fileName =>
            {
                var byteAsset = ResourceManager.LoadAsset<ByteAsset>($"Config/{fileName}.bin");
                return new ByteBuf(byteAsset.bytes);
            });
        }
    }
}
