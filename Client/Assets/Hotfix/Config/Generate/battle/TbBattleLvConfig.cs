
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Bright.Serialization;
using System.Collections.Generic;

namespace Hotfix.battle
{
   
/// <summary>
/// 
/// </summary>
public sealed partial class TbBattleLvConfig
{
    private readonly Dictionary<int, battle.BattleLvConfig> _dataMap;
    private readonly List<battle.BattleLvConfig> _dataList;
    
    public TbBattleLvConfig(ByteBuf _buf)
    {
        _dataMap = new Dictionary<int, battle.BattleLvConfig>();
        _dataList = new List<battle.BattleLvConfig>();
        
        for(int n = _buf.ReadSize() ; n > 0 ; --n)
        {
            battle.BattleLvConfig _v;
            _v = battle.BattleLvConfig.DeserializeBattleLvConfig(_buf);
            _dataList.Add(_v);
            _dataMap.Add(_v.Lv, _v);
        }
    }

    public Dictionary<int, battle.BattleLvConfig> DataMap => _dataMap;
    public List<battle.BattleLvConfig> DataList => _dataList;

    public battle.BattleLvConfig GetOrDefault(int key) => _dataMap.TryGetValue(key, out var v) ? v : null;
    public battle.BattleLvConfig Get(int key) => _dataMap[key];
    public battle.BattleLvConfig this[int key] => _dataMap[key];

    public void Resolve(Dictionary<string, object> _tables)
    {
        foreach(var v in _dataList)
        {
            v.Resolve(_tables);
        }
        OnResolveFinish(_tables);
    }


    partial void OnResolveFinish(Dictionary<string, object> _tables);
}

}
