
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Bright.Serialization;
using System.Collections.Generic;



namespace Hotfix.hero
{

/// <summary>
/// 
/// </summary>
public sealed partial class HeroConfig :  Bright.Config.BeanBase 
{
    public HeroConfig(ByteBuf _buf) 
    {
        Id = _buf.ReadInt();
        Name = _buf.ReadString();
        MainAttribute = (hero.HeroAttribute)_buf.ReadInt();
        HpBase = _buf.ReadInt();
        AtkBase = _buf.ReadInt();
        Atk2TowerBase = _buf.ReadInt();
        CriticalRateBase = _buf.ReadFloat();
        EvadeRateBase = _buf.ReadFloat();
        AttackTimeBase = _buf.ReadInt();
        HpUp = _buf.ReadInt();
        AtkUp = _buf.ReadInt();
        Atk2TowerUp = _buf.ReadInt();
        {int n = System.Math.Min(_buf.ReadSize(), _buf.Size);SpecialUp = new System.Collections.Generic.List<hero.HeroPropertySpecialUp>(n);for(var i = 0 ; i < n ; i++) { hero.HeroPropertySpecialUp _e;  _e = hero.HeroPropertySpecialUp.DeserializeHeroPropertySpecialUp(_buf); SpecialUp.Add(_e);}}
        Icon = _buf.ReadString();
        Spine = _buf.ReadString();
    }

    public HeroConfig(int Id, string Name, hero.HeroAttribute MainAttribute, int HpBase, int AtkBase, int Atk2TowerBase, float CriticalRateBase, float EvadeRateBase, int AttackTimeBase, int HpUp, int AtkUp, int Atk2TowerUp, System.Collections.Generic.List<hero.HeroPropertySpecialUp> SpecialUp, string Icon, string Spine ) 
    {
        this.Id = Id;
        this.Name = Name;
        this.MainAttribute = MainAttribute;
        this.HpBase = HpBase;
        this.AtkBase = AtkBase;
        this.Atk2TowerBase = Atk2TowerBase;
        this.CriticalRateBase = CriticalRateBase;
        this.EvadeRateBase = EvadeRateBase;
        this.AttackTimeBase = AttackTimeBase;
        this.HpUp = HpUp;
        this.AtkUp = AtkUp;
        this.Atk2TowerUp = Atk2TowerUp;
        this.SpecialUp = SpecialUp;
        this.Icon = Icon;
        this.Spine = Spine;
    }

    public static HeroConfig DeserializeHeroConfig(ByteBuf _buf)
    {
        return new hero.HeroConfig(_buf);
    }

    /// <summary>
    /// 
    /// </summary>
    public readonly int Id;
    /// <summary>
    /// 
    /// </summary>
    public readonly string Name;
    /// <summary>
    /// 
    /// </summary>
    public readonly hero.HeroAttribute MainAttribute;
    /// <summary>
    /// 
    /// </summary>
    public readonly int HpBase;
    /// <summary>
    /// 
    /// </summary>
    public readonly int AtkBase;
    /// <summary>
    /// 
    /// </summary>
    public readonly int Atk2TowerBase;
    /// <summary>
    /// 
    /// </summary>
    public readonly float CriticalRateBase;
    /// <summary>
    /// 
    /// </summary>
    public readonly float EvadeRateBase;
    /// <summary>
    /// 
    /// </summary>
    public readonly int AttackTimeBase;
    /// <summary>
    /// 
    /// </summary>
    public readonly int HpUp;
    /// <summary>
    /// 
    /// </summary>
    public readonly int AtkUp;
    /// <summary>
    /// 
    /// </summary>
    public readonly int Atk2TowerUp;
    /// <summary>
    /// 
    /// </summary>
    public readonly System.Collections.Generic.List<hero.HeroPropertySpecialUp> SpecialUp;
    /// <summary>
    /// 
    /// </summary>
    public readonly string Icon;
    /// <summary>
    /// 
    /// </summary>
    public readonly string Spine;

    public const int ID = -914282896;
    public override int GetTypeId() => ID;

    public  void Resolve(Dictionary<string, object> _tables)
    {
        foreach(var _e in SpecialUp) { _e?.Resolve(_tables); }
        OnResolveFinish(_tables);
    }

    partial void OnResolveFinish(Dictionary<string, object> _tables);

    public override string ToString()
    {
        return "{ "
        + "Id:" + Id + ","
        + "Name:" + Name + ","
        + "MainAttribute:" + MainAttribute + ","
        + "HpBase:" + HpBase + ","
        + "AtkBase:" + AtkBase + ","
        + "Atk2TowerBase:" + Atk2TowerBase + ","
        + "CriticalRateBase:" + CriticalRateBase + ","
        + "EvadeRateBase:" + EvadeRateBase + ","
        + "AttackTimeBase:" + AttackTimeBase + ","
        + "HpUp:" + HpUp + ","
        + "AtkUp:" + AtkUp + ","
        + "Atk2TowerUp:" + Atk2TowerUp + ","
        + "SpecialUp:" + Bright.Common.StringUtil.CollectionToString(SpecialUp) + ","
        + "Icon:" + Icon + ","
        + "Spine:" + Spine + ","
        + "}";
    }
    }

}

