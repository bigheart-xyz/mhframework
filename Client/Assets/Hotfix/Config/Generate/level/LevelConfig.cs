
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Bright.Serialization;
using System.Collections.Generic;



namespace Hotfix.level
{

/// <summary>
/// 
/// </summary>
public sealed partial class LevelConfig :  Bright.Config.BeanBase 
{
    public LevelConfig(ByteBuf _buf) 
    {
        Id = _buf.ReadInt();
        {int n = System.Math.Min(_buf.ReadSize(), _buf.Size);HeroFormation = new System.Collections.Generic.List<level.HeroRoadPos>(n);for(var i = 0 ; i < n ; i++) { level.HeroRoadPos _e;  _e = level.HeroRoadPos.DeserializeHeroRoadPos(_buf); HeroFormation.Add(_e);}}
        SpiritCoef = _buf.ReadFloat();
        OperateCoef = _buf.ReadInt();
        OutputCoef = _buf.ReadFloat();
        SurviveCoef = _buf.ReadFloat();
        SupportCoef = _buf.ReadFloat();
        GrowCoef = _buf.ReadFloat();
    }

    public LevelConfig(int Id, System.Collections.Generic.List<level.HeroRoadPos> HeroFormation, float SpiritCoef, int OperateCoef, float OutputCoef, float SurviveCoef, float SupportCoef, float GrowCoef ) 
    {
        this.Id = Id;
        this.HeroFormation = HeroFormation;
        this.SpiritCoef = SpiritCoef;
        this.OperateCoef = OperateCoef;
        this.OutputCoef = OutputCoef;
        this.SurviveCoef = SurviveCoef;
        this.SupportCoef = SupportCoef;
        this.GrowCoef = GrowCoef;
    }

    public static LevelConfig DeserializeLevelConfig(ByteBuf _buf)
    {
        return new level.LevelConfig(_buf);
    }

    /// <summary>
    /// 
    /// </summary>
    public readonly int Id;
    /// <summary>
    /// 
    /// </summary>
    public readonly System.Collections.Generic.List<level.HeroRoadPos> HeroFormation;
    /// <summary>
    /// 
    /// </summary>
    public readonly float SpiritCoef;
    /// <summary>
    /// 
    /// </summary>
    public readonly int OperateCoef;
    /// <summary>
    /// 
    /// </summary>
    public readonly float OutputCoef;
    /// <summary>
    /// 
    /// </summary>
    public readonly float SurviveCoef;
    /// <summary>
    /// 
    /// </summary>
    public readonly float SupportCoef;
    /// <summary>
    /// 
    /// </summary>
    public readonly float GrowCoef;

    public const int ID = 1091710044;
    public override int GetTypeId() => ID;

    public  void Resolve(Dictionary<string, object> _tables)
    {
        foreach(var _e in HeroFormation) { _e?.Resolve(_tables); }
        OnResolveFinish(_tables);
    }

    partial void OnResolveFinish(Dictionary<string, object> _tables);

    public override string ToString()
    {
        return "{ "
        + "Id:" + Id + ","
        + "HeroFormation:" + Bright.Common.StringUtil.CollectionToString(HeroFormation) + ","
        + "SpiritCoef:" + SpiritCoef + ","
        + "OperateCoef:" + OperateCoef + ","
        + "OutputCoef:" + OutputCoef + ","
        + "SurviveCoef:" + SurviveCoef + ","
        + "SupportCoef:" + SupportCoef + ","
        + "GrowCoef:" + GrowCoef + ","
        + "}";
    }
    }

}

