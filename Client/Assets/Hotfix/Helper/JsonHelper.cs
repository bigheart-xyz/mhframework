﻿using LitJson;

namespace Hotfix
{
	public static class JsonHelper
	{
		public static string ToJson(object obj)
		{
			return JsonMapper.ToJson(obj);
		}

		public static T FromJson<T>(string str)
		{
			var t = JsonMapper.ToObject<T>(str);
			if (!(t is ISupportInitialize iSupportInitialize))
			{
				return t;
			}
			iSupportInitialize.EndInit();
			return t;
		}

		public static T Clone<T>(T t)
		{
			return FromJson<T>(ToJson(t));
		}
	}
}