﻿using System;
using Model;
using UnityEngine;

namespace Hotfix
{

    public static class Init
    {
        public static GameObject GUI;
        
        public static void Start()
        {
            try
            {
                HotfixManager.Update = Update;
                HotfixManager.LateUpdate = LateUpdate;
                HotfixManager.OnApplicationQuit = OnApplicationQuit;

                ResourceManager.handleAction = UIManager.ResourceLoaded;
                
                EventSystem.Init();
                
                ConfigManager.Init();

                EventSystem.Run(EventIdType.InitSceneStart);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
        
        public static void Update()
        {
            try
            {
                EventSystem.Update();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public static void LateUpdate()
        {
            try
            {
                EventSystem.LateUpdate();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public static void OnApplicationQuit()
        {
        }
        
    }

}