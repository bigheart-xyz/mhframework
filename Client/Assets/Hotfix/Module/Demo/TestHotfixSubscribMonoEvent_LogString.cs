﻿using Model;
using UnityEngine;

namespace Hotfix
{
	// 分发数值监听
	[Event(Model.EventIdType.TestHotfixSubscribMonoEvent)]
    // ReSharper disable once UnusedMember.Global
    // ReSharper disable once InconsistentNaming
    public class TestHotfixSubscribMonoEvent_LogString : AEvent<string>
	{
		public override void Run(string info)
		{
			Debug.Log(info);
		}
	}
}
