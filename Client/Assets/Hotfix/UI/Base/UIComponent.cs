﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Model;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Hotfix
{
    public enum PCType
    {
        Panel,
        Pop,
        Cpt
    }
        
    [ObjectSystem]
    public class UiComponentSystem : AwakeSystem<UIComponent, string, GameObject>
    {
        public override void Awake(UIComponent self, string name, GameObject go)
        {
            self.MAwake(name, go);
        }
    }

    public class UIComponent : Component
    {
        public virtual PCType pcType { get; } = PCType.Panel;

        public CanvasGroup panel;
        public RectTransform transform;
        public GameObject gameObject;
        public ReferenceCollector rc;
        
        public string Name { get; set; }
        public System.Object[] Data { get; set; }

        private Sequence tween;
        public List<AsyncOperationHandle> resHandles;

        public virtual void Awake()
        {
        }
        
        public virtual void Start()
        {
        }
        
        public virtual void ApplyInfo()
        {
        }
        
        public virtual void OnPanelShow()
        {
        }
        
        public virtual void OnPanelHide()
        {
        }

        public virtual void SetActive(bool active, Action action = null)
        {
            if (pcType == PCType.Panel)
            {
                MFadePanel(active, action);
            }
            else if (pcType == PCType.Pop)
            {
                MPopPanel(active, action);
            }
            else if (pcType == PCType.Cpt)
            {
                MSetActive(active, action);
            }
            else
            {
                Debug.LogError($"{Name}'s pcType define error!");
            }
        }

        public virtual void Hide(Action action = null)
        {
            SetActive(false, action);
        }

        public virtual void Back()
        {
            if (pcType == PCType.Panel)
            {
                UIManager.BackToLastPanel();
            }
            else if (pcType == PCType.Pop)
            {
                UIManager.BackToLastPop();
            }
        }

        public virtual void PanelFaded(System.Object[] data)
        {
            Data = data;
            ApplyInfo();
            SetActive(true);
        }

        public virtual void PanelBacked()
        {
        }

        public virtual void PanelDestroyed()
        {
        }

        public virtual T Get<T>(string key) where T : class
        {
            return rc.Get<T>(key);
        }

        public virtual T Find<T>(string path)
        {
            return transform.Find(path).GetComponent<T>();
        }

        public virtual GameObject Find(string path)
        {
            return transform.Find(path).gameObject;
        }
        
        
        public void Destroy(bool force = false)
        {
            if (force)
            {
                ReleaseRes();
            }
            else
            {
                Hide(ReleaseRes);
            }
        }

        public void MAwake(string name, GameObject go)
        {
            Name = name;
            
            gameObject = go;
            transform = go.transform as RectTransform;
            System.Diagnostics.Debug.Assert(transform != null, nameof(transform) + " != null");
            panel = transform.GetComponent<CanvasGroup>();
            rc = transform.GetComponent<ReferenceCollector>();
            
            Awake();
        }

        public void MSetActive(bool active, Action action = null)
        {
            if (gameObject.activeSelf != active)
            {
                gameObject.SetActive(active);
            }
            
            action?.Invoke();
        }

        private void MFadePanel(bool active, Action action = null)
        {
            if (tween.IsActive() && tween.IsPlaying())
            {
                tween.Complete(true);
                tween = null;
            }

            if (active && gameObject.activeSelf && Math.Abs(panel.alpha - 1) < 0.000001)
            {
                MOnPanelShow();
                action?.Invoke();
                return;
            }
            else if (!active && !gameObject.activeSelf)
            {
                MOnPanelHide();
                action?.Invoke();
                return;
            }
            
            LoadingManager.Instance.SetBreakInput(true);

            Sequence sq = DOTween.Sequence();
            if (active)
            {
                gameObject.SetActive(true);
                panel.alpha = 0;
                sq.Append(panel.DOFade(1, 0.4f));
                sq.SetEase(Ease.InSine).OnComplete(() =>
                {
                    LoadingManager.Instance.SetBreakInput(false);
                    MOnPanelShow();
                    action?.Invoke();
                });
            }
            else
            {
                panel.alpha = 1;
                sq.Append(panel.DOFade(0, 0.4f));
                sq.SetEase(Ease.OutSine).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    LoadingManager.Instance.SetBreakInput(false);
                    MOnPanelHide();
                    action?.Invoke();
                });
            }

            tween = sq;
        }

        private void MPopPanel(bool active, Action action = null)
        {
            if (tween.IsActive() && tween.IsPlaying())
            {
                tween.Complete(true);
                tween = null;
            }
            
            if (active && gameObject.activeSelf && Math.Abs(panel.alpha - 1) < 0.000001)
            {
                MOnPanelShow();
                action?.Invoke();
                return;
            }
            else if (!active && !gameObject.activeSelf)
            {
                MOnPanelHide();
                action?.Invoke();
                return;
            }
            
            LoadingManager.Instance.SetBreakInput(true);
            
            Sequence sq = DOTween.Sequence();
            if (active)
            {
                gameObject.SetActive(true);
                panel.alpha = 0;
                transform.localScale = new Vector3(0.8f,0.8f, 0.8f);
                sq.Append(transform.DOScale(1, 0.4f));
                sq.Insert(0, panel.DOFade(1, 0.4f));
                sq.SetEase(Ease.OutSine).OnComplete(() =>
                {
                    LoadingManager.Instance.SetBreakInput(false);
                    MOnPanelShow();
                    action?.Invoke();
                });
            }
            else
            {
                panel.alpha = 1;
                sq.Append(transform.DOScale(1.1f, 0.4f));
                sq.Insert(0, panel.DOFade(0, 0.4f));
                sq.SetEase(Ease.OutSine).OnComplete(() =>
                {
                    gameObject.SetActive(false);
                    LoadingManager.Instance.SetBreakInput(false);
                    MOnPanelHide();
                    action?.Invoke();
                });
            }

            tween = sq;
        }

        private void MOnPanelShow()
        {
            OnPanelShow();
        }

        private void MOnPanelHide()
        {
            OnPanelHide();
        }

        private void ReleaseRes()
        {
            foreach (var handle in resHandles)
            {
                Model.ResourceManager.ReleaseAsset(handle);
            }
            resHandles.Clear();
            resHandles = null;

            MDestroyPanel();
        }
        
        private void MDestroyPanel()
        {
            UnityEngine.Object.Destroy(gameObject);
            PanelDestroyed();

            Data = null;
        }


        public static T Create<T>(string name, GameObject go) where T : UIComponent
        {
            return ComponentFactory.Create<T, string, GameObject>(name, go);
        }
        
        public static UIComponent Create(Type t, string name, GameObject go)
        {
            return (UIComponent) ComponentFactory.Create(t, name, go);
        }
    }

}