﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Hotfix
{
    public struct UIFadeData
    {
        public int type;
        public string name;
        public int extraArg;
        public System.Object[] data;
    }
    
    public static class UIManager
    {
        public static UIPanel currentPanel;
        public static ListStack<UIPanel> panelStack = new ListStack<UIPanel>();

        private static List<AsyncOperationHandle> resHandles;
        
        private static int fadingType;
        private static int fadeSetPos;
        private static List<UIFadeData> fadeCache = new List<UIFadeData>();

        private static void SetOp(int type, string name, int extraArg, params System.Object[] data)
        {
            var op = new UIFadeData() {type = type, name = name, extraArg = extraArg, data = data};
            fadeCache.Insert(fadeSetPos, op);
        }

        private static void CheckOp()
        {
            fadingType = 0;
            fadeSetPos = 0;
            
            if (fadeCache.Count == 0) return;
            
            var op = fadeCache[0];
            fadeCache.RemoveAt(0);
            switch (op.type)
            {
                case 1:
                    FadeToPanelImp(op.extraArg, op.name, op.data);
                    break;
                case 2:
                    PopupPanel(op.name, op.data);
                    break;
                case 3:
                    BackToLastPanel();
                    break;
                case 4:
                    BackToPanelByName(op.name);
                    break;
                case 5:
                    BackToLastPop();
                    break;
                case 6:
                    BackToPopByName(op.name);
                    break;
            }
        }

        public static void FadeToPanel(string name, params System.Object[] data)
        {
            FadeToPanelImp(1, name, data);
        }
        
        public static void FadeToPanelAfterAll(string name, params System.Object[] data)
        {
            FadeToPanelImp(2, name, data);
        }

        private static async void FadeToPanelImp(int type, string name, params System.Object[] data)
        {
            if (fadingType > 0)
            {
                SetOp(1, name, type, data);
                return;
            }

            fadingType = 1;
            var isCurrent = currentPanel?.panel.Name == name;
            if (isCurrent)
            {
                if (type == 2)
                {
                    var tempCurrent = currentPanel;
                    panelStack.Pop();
                    BackToPanelByName(UIName.MainPanel, false);
                    panelStack.Push(tempCurrent);
                    currentPanel = tempCurrent;
                }
                
                currentPanel?.panel.PanelFaded(data);
                CheckOp();
            }
            else
            {
                if (type == 2)
                {
                    BackToPanelByName(UIName.MainPanel, false);
                }
                
                currentPanel?.Hide();
                resHandles = new List<AsyncOperationHandle>();
                var go = await Model.UIFactory.Create(null, name);
                var t = EventSystem.GetUICompoent(name);
                var cpt = UIComponent.Create(t, name, go);
                var panel = new UIPanel(cpt);
                panelStack.Push(panel);
                currentPanel = panel;
                panel.PanelFaded(data, resHandles);
                resHandles = null;
                CheckOp();
            }
        }

        public static async void PopupPanel(string name, params System.Object[] data)
        {
            if (fadingType > 0)
            {
                SetOp(2, name, 0, data);
                return;
            }

            if (currentPanel == null) return;

            fadingType = 2;
            if (currentPanel.currentPop != null && currentPanel.currentPop.Name == name)
            {
                currentPanel.currentPop.PanelFaded(data);
                CheckOp();
            }
            else
            {
                resHandles = new List<AsyncOperationHandle>();
                var go = await Model.UIFactory.Create(currentPanel.panel.transform, name);
                var t = EventSystem.GetUICompoent(name);
                var cpt = (UIComponent) ComponentFactory.Create(t, name, go);
                currentPanel.PopFaded(cpt, data, resHandles);
                resHandles = null;
                CheckOp();
            }
        }

        public static void BackToLastPanel()
        {
            if (fadingType > 0)
            {
                SetOp(3, null, 0, null);
                return;
            }

            fadingType = 3;
            
            currentPanel?.Destroy();

            panelStack.Pop();
            currentPanel = panelStack.Peek();
            currentPanel.PanelBacked();
            CheckOp();
        }

        public static void BackToPanelByName(string name, bool realBack = true)
        {
            if (realBack && fadingType > 0)
            {
                SetOp(4, name, 0, null);
                return;
            }

            if (realBack)
            {
                fadingType = 4;
            }

            while (currentPanel != null && currentPanel.panel.Name != name)
            {
                currentPanel.Destroy();
                panelStack.Pop();
                currentPanel = panelStack.Peek();
            }

            if (realBack)
            {
                currentPanel?.PanelBacked();
                if (currentPanel == null) Debug.LogError($"想要back的界面不存在，请检查！！！ panelName = {name}");
                CheckOp();
            }
        }

        public static void ClearPanels()
        {
            while (currentPanel != null)
            {
                currentPanel.Destroy(true);
                panelStack.Pop();
                currentPanel = panelStack.Peek();
            }
        }

        public static void BackToLastPop()
        {
            if (fadingType > 0)
            {
                SetOp(5, null, 0, null);
                return;
            }
            
            if (currentPanel == null) return;

            fadingType = 5;
            currentPanel.PopBacked();
            CheckOp();
        }

        public static void BackToPopByName(string name)
        {
            if (fadingType > 0)
            {
                SetOp(6, null, 0, null);
                return;
            }
            
            if (currentPanel == null) return;

            fadingType = 6;
            currentPanel.PopBackedByName(name);
            CheckOp();
        }

        public static void BackToCurrentPanel()
        {
            BackToPopByName(null);
        }

        public static bool IsPanelExist(string name)
        {
            for (int i = panelStack.Count-1; i >= 0; i--)
            {
                if (panelStack[i].panel.Name == name) return true;
            }

            return false;
        }

        public static UIPanel GetPanel(string name)
        {
            for (int i = panelStack.Count-1; i >= 0; i--)
            {
                if (panelStack[i].panel.Name == name) return panelStack[i];
            }

            return null;
        }

        public static UIPanel GetCurrentPanel()
        {
            return currentPanel;
        }
        
        public static UIComponent GetPanelComponent(string name)
        {
            var panel = GetPanel(name);
            return panel?.panel;
        }

        public static UIComponent GetPopComponent(string panelName, string popName)
        {
            var panel = GetPanel(panelName);
            return panel.GetPop(popName);
        }

        public static UIComponent GetCurrentPop(string name)
        {
            return currentPanel?.GetPop(name);
        }



        public static void ResourceLoaded(AsyncOperationHandle handle)
        {
            var handles = resHandles;
            if (handles == null)
            {
                if (currentPanel != null)
                {
                    handles = currentPanel.currentPop == null ? currentPanel.panel.resHandles : currentPanel.currentPop.resHandles;
                }
            }
            handles?.Add(handle);
        }
        
    }
}
