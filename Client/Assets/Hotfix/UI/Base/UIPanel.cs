﻿using System.Collections.Generic;
using Model;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Hotfix
{
    public class UIPanel
    {
        public UIComponent panel;

        public UIComponent currentPop;
        public readonly ListStack<UIComponent> popStack;

        public UIPanel(UIComponent cpt)
        {
            panel = cpt;
            
            popStack = new ListStack<UIComponent>();
        }

        public void PopFaded(UIComponent pop, System.Object[] data, List<AsyncOperationHandle> resHandles)
        {
            popStack.Push(pop);
            currentPop = pop;
            
            //把遮挡面板的depth提升
            PopUpMaskManager.SetDepth(pop.transform);

            pop.resHandles = resHandles;
            pop.PanelFaded(data);
        }

        public void PopBacked()
        {
            if (currentPop == null) return;

            currentPop.Destroy();
            popStack.Pop();
            currentPop = popStack.Peek();
            
            PopUpMaskManager.SetDepth(currentPop?.transform);

            if (currentPop != null)
            {
                currentPop.PanelBacked();
            }
            else
            {
                panel.PanelBacked();
            }
        }

        public void PopBackedByName(string name)
        {
            if (currentPop == null) return;

            while (currentPop != null && currentPop.Name != name)
            {
                currentPop.Destroy();
                popStack.Pop();
                currentPop = popStack.Peek();
            }
            
            PopUpMaskManager.SetDepth(currentPop?.transform);
            
            if (currentPop != null)
            {
                currentPop.PanelBacked();
            }
            else
            {
                panel.PanelBacked();
            }
        }
        
        public void PanelFaded(System.Object[] data, List<AsyncOperationHandle> resHandles)
        {
            PopUpMaskManager.SetDepth(currentPop?.transform);
            
            panel.resHandles = resHandles;
            panel.PanelFaded(data);
        }

        public void PanelBacked()
        {
            PopUpMaskManager.SetDepth(currentPop?.transform);
            
            panel.PanelBacked();
            for (int i = 0, imax = popStack.Count; i < imax; i++)
            {
                popStack[i].SetActive(true);
            }
            currentPop?.PanelBacked();
        }

        public void Hide()
        {
            for (int i = popStack.Count-1; i >= 0; i--)
            {
                popStack[i].MSetActive(false);
            }
            panel.Hide();
        }
        
        public void Destroy(bool force = false)
        {
            for (int i = popStack.Count-1; i >= 0; i--)
            {
                popStack[i].MSetActive(false);
                popStack[i].Destroy(force);
            }
            panel.Destroy(force);
        }

        public UIComponent GetPop(string name)
        {
            for (int i = popStack.Count-1; i >= 0; i--)
            {
                if (popStack[i].Name == name) return popStack[i];
            }

            return null;
        }
    }
}
