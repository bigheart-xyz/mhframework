﻿using Model;
using UnityEngine;
using UnityEngine.UI;

namespace Hotfix
{
    [ObjectSystem]
    public class LoginPanelCptSystem : AwakeSystem<LoginPanelCpt, string, GameObject>
    {
        public override void Awake(LoginPanelCpt self, string name, GameObject go)
        {
            self.MAwake(name, go);
        }
    }
    
    [UI(UIName.LoginPanel)]
    public class LoginPanelCpt : UIComponent
    {
        public override PCType pcType { get; } = PCType.Panel;

        private Text titleText;
        private Button loginButton;
        
        public override void Awake()
        {
            Debug.Log("Awake");
            titleText = Get<Text>("Title");
            loginButton = Get<Button>("LoginButton");
            
            loginButton.onClick.AddListener(LoginButtonClick);
        }
        
        public override void ApplyInfo()
        {
            Debug.Log("ApplyInfo");
            titleText.text = "未登陆";
        }

        private void LoginButtonClick()
        {
            Debug.Log("LoginButtonClick!!!");
        }
    }
}
