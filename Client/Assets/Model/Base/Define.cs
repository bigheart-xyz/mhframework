﻿namespace Model
{
    public static class Define
    {
        public const string SoundPath = "Sound/";
        public const string VideoPath = "Video/";
        public const string ConfigPath = "Config/";
        public const string ScenePath = "Map/";
        public const string TexturePath = "Textures/";
        public const string AtlasPath = "UIAtlas/";
        public const string UIPrefabPath = "Prefabs/UI/";
        public const string CharacterPrefabPath = "Prefabs/Characters/";
        public const string ParticlePrefabPath = "Prefabs/Particles/";
        
        public const string DotPrefab = ".prefab";
        public const string DotPng = ".png";
        public const string DotSpriteAtlas = ".spriteatlas";

        public const string InitScene = "Init";
        public const string EmptyScene = "Empty";
        public const string MainScene = "Main";
        public const string BattleScene = "Battle";

        public const string GUI = "GUI";
        public const string PopUpMaskPanel = "PopUpMaskPanel";
    }
}
