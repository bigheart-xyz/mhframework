﻿namespace Model
{
	public static class EventIdType
	{
		public const string TestHotfixSubscribMonoEvent = "TestHotfixSubscribMonoEvent";
		public const string LoadingStart = "LoadingStart";
	}
}