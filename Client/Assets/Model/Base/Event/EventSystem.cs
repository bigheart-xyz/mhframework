﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Model
{
    public enum DLLType
    {
        Model,
        Hotfix,
        Editor,
    }

    public static class EventSystem
    {
	    private static readonly Dictionary<long, Component> allComponents = new Dictionary<long, Component>();
	    
        private static readonly Dictionary<DLLType, Assembly> assemblies = new Dictionary<DLLType, Assembly>();
        private static readonly UnOrderMultiMap<Type, Type> types = new UnOrderMultiMap<Type, Type>();
        
        private static readonly Dictionary<string, List<IEvent>> allEvents = new Dictionary<string, List<IEvent>>();
        
        private static readonly UnOrderMultiMap<Type, IAwakeSystem> awakeSystems = new UnOrderMultiMap<Type, IAwakeSystem>();

        private static readonly UnOrderMultiMap<Type, IStartSystem> startSystems = new UnOrderMultiMap<Type, IStartSystem>();

        private static readonly UnOrderMultiMap<Type, IDestroySystem> destroySystems = new UnOrderMultiMap<Type, IDestroySystem>();

        private static readonly UnOrderMultiMap<Type, ILoadSystem> loadSystems = new UnOrderMultiMap<Type, ILoadSystem>();

        private static readonly UnOrderMultiMap<Type, IUpdateSystem> updateSystems = new UnOrderMultiMap<Type, IUpdateSystem>();

        private static readonly UnOrderMultiMap<Type, ILateUpdateSystem> lateUpdateSystems = new UnOrderMultiMap<Type, ILateUpdateSystem>();

        private static readonly UnOrderMultiMap<Type, IDeserializeSystem> deserializeSystems = new UnOrderMultiMap<Type, IDeserializeSystem>();
        
        private static Queue<long> updates = new Queue<long>();
        private static Queue<long> updates2 = new Queue<long>();
		
        private static readonly Queue<long> starts = new Queue<long>();

        private static Queue<long> loaders = new Queue<long>();
        private static Queue<long> loaders2 = new Queue<long>();

        private static Queue<long> lateUpdates = new Queue<long>();
        private static Queue<long> lateUpdates2 = new Queue<long>();
        
        public static void Add(DLLType dllType, Assembly assembly)
		{
			assemblies[dllType] = assembly;
			types.Clear();
			foreach (var value in assemblies.Values)
			{
				foreach (var type in value.GetTypes())
				{
					object[] objects = type.GetCustomAttributes(typeof(BaseAttribute), false);
					if (objects.Length == 0)
					{
						continue;
					}

					var baseAttribute = (BaseAttribute) objects[0];
					types.Add(baseAttribute.AttributeType, type);
				}
			}

			awakeSystems.Clear();
			lateUpdateSystems.Clear();
			updateSystems.Clear();
			startSystems.Clear();
			loadSystems.Clear();
			destroySystems.Clear();
			deserializeSystems.Clear();

			if (types.ContainsKey(typeof(ObjectSystemAttribute)))
			{
				foreach (var type in types[typeof(ObjectSystemAttribute)])
				{
					object[] attrs = type.GetCustomAttributes(typeof(ObjectSystemAttribute), false);

					if (attrs.Length == 0)
					{
						continue;
					}

					object obj = Activator.CreateInstance(type);

					switch (obj)
					{
						case IAwakeSystem objectSystem:
							awakeSystems.Add(objectSystem.Type(), objectSystem);
							break;
						case IUpdateSystem updateSystem:
							updateSystems.Add(updateSystem.Type(), updateSystem);
							break;
						case ILateUpdateSystem lateUpdateSystem:
							lateUpdateSystems.Add(lateUpdateSystem.Type(), lateUpdateSystem);
							break;
						case IStartSystem startSystem:
							startSystems.Add(startSystem.Type(), startSystem);
							break;
						case IDestroySystem destroySystem:
							destroySystems.Add(destroySystem.Type(), destroySystem);
							break;
						case ILoadSystem loadSystem:
							loadSystems.Add(loadSystem.Type(), loadSystem);
							break;
						case IDeserializeSystem deserializeSystem:
							deserializeSystems.Add(deserializeSystem.Type(), deserializeSystem);
							break;
					}
				}
			}

			allEvents.Clear();
			if (types.ContainsKey(typeof(EventAttribute)))
			{
				foreach (var type in types[typeof(EventAttribute)])
				{
					object[] attrs = type.GetCustomAttributes(typeof(EventAttribute), false);

					foreach (object attr in attrs)
					{
						var aEventAttribute = (EventAttribute)attr;
						object obj = Activator.CreateInstance(type);
						var iEvent = obj as IEvent;
						if (iEvent == null)
						{
							Debug.LogError($"{obj.GetType().Name} 没有继承IEvent");
						}
						RegisterEvent(aEventAttribute.Type, iEvent);
					}
				}
			}
			
			Load();
		}
        
        public static void RegisterEvent(string eventId, IEvent e)
        {
	        if (!allEvents.ContainsKey(eventId))
	        {
		        allEvents.Add(eventId, new List<IEvent>());
	        }
	        allEvents[eventId].Add(e);
        }
        
        public static Assembly Get(DLLType dllType)
        {
	        return assemblies[dllType];
        }
		
        public static List<Type> GetTypes(Type systemAttributeType)
        {
	        if (!types.ContainsKey(systemAttributeType))
	        {
		        return new List<Type>();
	        }
	        return types[systemAttributeType];
        }
        
        public static void Add(Component component)
        {
	        allComponents.Add(component.InstanceId, component);

	        var type = component.GetType();

	        if (loadSystems.ContainsKey(type))
	        {
		        loaders.Enqueue(component.InstanceId);
	        }

	        if (updateSystems.ContainsKey(type))
	        {
		        updates.Enqueue(component.InstanceId);
	        }

	        if (startSystems.ContainsKey(type))
	        {
		        starts.Enqueue(component.InstanceId);
	        }

	        if (lateUpdateSystems.ContainsKey(type))
	        {
		        lateUpdates.Enqueue(component.InstanceId);
	        }
        }
        
        public static void Remove(long instanceId)
        {
	        allComponents.Remove(instanceId);
        }
        
        public static Component Get(long instanceId)
        {
	        allComponents.TryGetValue(instanceId, out var component);
	        return component;
        }
        
        public static void Deserialize(Component component)
        {
	        var iDeserializeSystems = deserializeSystems[component.GetType()];
	        if (iDeserializeSystems == null)
	        {
		        return;
	        }

	        foreach (var deserializeSystem in iDeserializeSystems)
	        {
		        if (deserializeSystem == null)
		        {
			        continue;
		        }

		        try
		        {
			        deserializeSystem.Run(component);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }
        
        public static void Awake(Component component)
		{
			var iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}

			foreach (var aAwakeSystem in iAwakeSystems)
			{
				if (aAwakeSystem == null)
				{
					continue;
				}
				
				var iAwake = aAwakeSystem as IAwake;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}

		public static void Awake<P1>(Component component, P1 p1)
		{
			var iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}

			foreach (var aAwakeSystem in iAwakeSystems)
			{
				if (aAwakeSystem == null)
				{
					continue;
				}
				
				var iAwake = aAwakeSystem as IAwake<P1>;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component, p1);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}

		public static void Awake<P1, P2>(Component component, P1 p1, P2 p2)
		{
			var iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}

			foreach (var aAwakeSystem in iAwakeSystems)
			{
				if (aAwakeSystem == null)
				{
					continue;
				}
				
				var iAwake = aAwakeSystem as IAwake<P1, P2>;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component, p1, p2);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}

		public static void Awake<P1, P2, P3>(Component component, P1 p1, P2 p2, P3 p3)
		{
			var iAwakeSystems = awakeSystems[component.GetType()];
			if (iAwakeSystems == null)
			{
				return;
			}

			foreach (var aAwakeSystem in iAwakeSystems)
			{
				if (aAwakeSystem == null)
				{
					continue;
				}

				var iAwake = aAwakeSystem as IAwake<P1, P2, P3>;
				if (iAwake == null)
				{
					continue;
				}

				try
				{
					iAwake.Run(component, p1, p2, p3);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}
		
		public static void Load()
		{
			while (loaders.Count > 0)
			{
				long instanceId = loaders.Dequeue();
				if (!allComponents.TryGetValue(instanceId, out var component))
				{
					continue;
				}
				if (component.IsDisposed)
				{
					continue;
				}
				
				var iLoadSystems = loadSystems[component.GetType()];
				if (iLoadSystems == null)
				{
					continue;
				}

				loaders2.Enqueue(instanceId);

				foreach (var iLoadSystem in iLoadSystems)
				{
					try
					{
						iLoadSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}

			ObjectHelper.Swap(ref loaders, ref loaders2);
		}
		
		private static void Start()
		{
			while (starts.Count > 0)
			{
				long instanceId = starts.Dequeue();
				if (!allComponents.TryGetValue(instanceId, out var component))
				{
					continue;
				}

				var iStartSystems = startSystems[component.GetType()];
				if (iStartSystems == null)
				{
					continue;
				}
				
				foreach (var iStartSystem in iStartSystems)
				{
					try
					{
						iStartSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}
		}
		
		public static void Destroy(Component component)
		{
			var iDestroySystems = destroySystems[component.GetType()];
			if (iDestroySystems == null)
			{
				return;
			}

			foreach (var iDestroySystem in iDestroySystems)
			{
				if (iDestroySystem == null)
				{
					continue;
				}

				try
				{
					iDestroySystem.Run(component);
				}
				catch (Exception e)
				{
					Debug.LogError(e);
				}
			}
		}
		
		public static void Update()
		{
			Start();
			
			while (updates.Count > 0)
			{
				long instanceId = updates.Dequeue();
				if (!allComponents.TryGetValue(instanceId, out var component))
				{
					continue;
				}
				if (component.IsDisposed)
				{
					continue;
				}
				
				var iUpdateSystems = updateSystems[component.GetType()];
				if (iUpdateSystems == null)
				{
					continue;
				}

				updates2.Enqueue(instanceId);

				foreach (var iUpdateSystem in iUpdateSystems)
				{
					try
					{
						iUpdateSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}

			ObjectHelper.Swap(ref updates, ref updates2);
		}

		public static void LateUpdate()
		{
			while (lateUpdates.Count > 0)
			{
				long instanceId = lateUpdates.Dequeue();
				if (!allComponents.TryGetValue(instanceId, out var component))
				{
					continue;
				}
				if (component.IsDisposed)
				{
					continue;
				}

				var iLateUpdateSystems = lateUpdateSystems[component.GetType()];
				if (iLateUpdateSystems == null)
				{
					continue;
				}

				lateUpdates2.Enqueue(instanceId);

				foreach (var iLateUpdateSystem in iLateUpdateSystems)
				{
					try
					{
						iLateUpdateSystem.Run(component);
					}
					catch (Exception e)
					{
						Debug.LogError(e);
					}
				}
			}

			ObjectHelper.Swap(ref lateUpdates, ref lateUpdates2);
		}

		public static void Run(string type)
        {
	        if (!allEvents.TryGetValue(type, out var iEvents))
	        {
		        return;
	        }
	        foreach (var iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle();
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }

        public static void Run<A>(string type, A a)
        {
	        if (!allEvents.TryGetValue(type, out var iEvents))
	        {
		        return;
	        }
	        foreach (var iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle(a);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }

        public static void Run<A, B>(string type, A a, B b)
        {
	        if (!allEvents.TryGetValue(type, out var iEvents))
	        {
		        return;
	        }
	        foreach (var iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle(a, b);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }

        public static void Run<A, B, C>(string type, A a, B b, C c)
        {
	        if (!allEvents.TryGetValue(type, out var iEvents))
	        {
		        return;
	        }
	        foreach (var iEvent in iEvents)
	        {
		        try
		        {
			        iEvent?.Handle(a, b, c);
		        }
		        catch (Exception e)
		        {
			        Debug.LogError(e);
		        }
	        }
        }
        
    }

}
