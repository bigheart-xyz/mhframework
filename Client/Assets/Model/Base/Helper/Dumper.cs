﻿using System;
using System.Collections;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Model
{
    public static class Dumper
    {
        private static readonly StringBuilder s_text = new StringBuilder("", 1024);

        private static void AppendIndent(int num)
        {
            s_text.Append(' ', num);
        }

        private static void DoDump(object obj)
        {
            if (obj == null)
            {
                s_text.Append("null");
                s_text.Append(",");
                return;
            }

            var t = obj.GetType();

            //repeat field
            if (obj is IList list)
            {
                /*
                _text.Append(t.FullName);
                _text.Append(",");
                AppendIndent(1);
                */

                s_text.Append("[");
                foreach (object v in list)
                {
                    DoDump(v);
                }

                s_text.Append("]");
            }
            else if (t.IsValueType)
            {
                s_text.Append(obj);
                s_text.Append(",");
                AppendIndent(1);
            }
            else if (obj is string)
            {
                s_text.Append("\"");
                s_text.Append(obj);
                s_text.Append("\"");
                s_text.Append(",");
                AppendIndent(1);
            }
            else if (t.IsArray)
            {
                var a = (Array) obj;
                s_text.Append("[");
                for (int i = 0; i < a.Length; i++)
                {
                    s_text.Append(i);
                    s_text.Append(":");
                    DoDump(a.GetValue(i));
                }

                s_text.Append("]");
            }
            else if (t.IsClass)
            {
                s_text.Append($"<{t.Name}>");
                s_text.Append("{");
                var fields = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                if (fields.Length > 0)
                {
                    foreach (var info in fields)
                    {
                        s_text.Append(info.Name);
                        s_text.Append(":");
                        object value = info.GetGetMethod().Invoke(obj, null);
                        DoDump(value);
                    }
                }

                s_text.Append("}");
            }
            else
            {
                Debug.LogWarning("unsupport type: " + t.FullName);
                s_text.Append(obj);
                s_text.Append(",");
                AppendIndent(1);
            }
        }

        public static string DumpAsString(object obj, string hint = "")
        {
            s_text.Clear();
            s_text.Append(hint);
            DoDump(obj);
            return s_text.ToString();
        }
    }
}