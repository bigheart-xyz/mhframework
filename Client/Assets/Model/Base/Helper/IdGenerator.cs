﻿
namespace Model
{
    public static class IdGenerator
    {
        private static long s_instanceIdValue;
        public static long GenerateInstanceId()
        {
            return ++s_instanceIdValue;
        }
    }
}
