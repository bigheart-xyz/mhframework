﻿using System;

namespace Model
{
	public static class Log
	{
		public static void Info(string msg)
		{
			UnityEngine.Debug.Log(msg);
		}

		public static void Warning(string msg)
		{
			UnityEngine.Debug.LogWarning(msg);
		}

		public static void Error(string msg)
		{
			UnityEngine.Debug.LogError(msg);
		}
		
		public static void Error(Exception e)
		{
			UnityEngine.Debug.LogException(e);
		}

		public static void Fatal(string msg)
		{
			UnityEngine.Debug.LogAssertion(msg);
		}

		public static void Msg(object msg)
		{
			Info(Dumper.DumpAsString(msg));
		}
	}
}