﻿using System;
using System.Threading.Tasks;

namespace Model
{
	public abstract class Component : Object, IDisposable
	{
		public long InstanceId { get; private set; }

		public bool IsDisposed => this.InstanceId == 0;

		public Component Parent { get; set; }

		public T GetParent<T>() where T : Component
		{
			return this.Parent as T;
		}

		protected Component()
		{
			this.InstanceId = IdGenerator.GenerateInstanceId();
		}


		public virtual void Dispose()
		{
			if (this.IsDisposed)
			{
				return;
			}
			
			// 触发Destroy事件
			EventSystem.Destroy(this);

			EventSystem.Remove(this.InstanceId);
			
			this.InstanceId = 0;
		}

		public override async Task EndInit()
		{
			EventSystem.Deserialize(this);

			await Task.CompletedTask;
		}
	}
}