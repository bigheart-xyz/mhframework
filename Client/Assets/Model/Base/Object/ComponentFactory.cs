﻿using System;

namespace Model
{
	public static class ComponentFactory
	{
		public static Component CreateWithParent(Type type, Component parent)
		{
			var component = (Component)Activator.CreateInstance(type);

			EventSystem.Add(component);

			component.Parent = parent;
			
			EventSystem.Awake(component);
			return component;
		}

		public static T CreateWithParent<T>(Component parent) where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			
			component.Parent = parent;

			EventSystem.Awake(component);
			return component;
		}

		public static T CreateWithParent<T, A>(Component parent, A a) where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			
			component.Parent = parent;

			EventSystem.Awake(component, a);
			return component;
		}

		public static T CreateWithParent<T, A, B>(Component parent, A a, B b) where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			
			component.Parent = parent;

			EventSystem.Awake(component, a, b);
			return component;
		}

		public static T CreateWithParent<T, A, B, C>(Component parent, A a, B b, C c) where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			
			component.Parent = parent;

			EventSystem.Awake(component, a, b, c);
			return component;
		}

		public static T Create<T>() where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			EventSystem.Awake(component);
			return component;
		}

		public static T Create<T, A>(A a) where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			EventSystem.Awake(component, a);
			return component;
		}

		public static T Create<T, A, B>(A a, B b) where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			EventSystem.Awake(component, a, b);
			return component;
		}

		public static T Create<T, A, B, C>(A a, B b, C c) where T : Component
		{
			var type = typeof (T);
			
			var component = (T)Activator.CreateInstance(type);

			EventSystem.Add(component);
			EventSystem.Awake(component, a, b, c);
			return component;
		}
	}
}
