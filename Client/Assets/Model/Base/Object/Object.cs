﻿using System.Threading.Tasks;

namespace Model
{
	public interface ISupportInitialize
	{
		Task BeginInit();
		Task EndInit();
	}
	
	public abstract class Object: ISupportInitialize
	{
		public virtual async Task BeginInit()
		{
			await Task.CompletedTask;
		}

		public virtual async Task EndInit()
		{
			await Task.CompletedTask;
		}
	}
}