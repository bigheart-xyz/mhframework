﻿using UnityEngine;

public class ByteAsset : ScriptableObject
{
    public byte[] bytes;

    public ByteAsset(byte[] bytes)
    {
        this.bytes = bytes;
    }
}
