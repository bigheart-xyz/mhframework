﻿using System;
using UnityEngine;

namespace Model
{
    public class Init : MonoBehaviour
    {
        private void Start()
        {
            StartAsync();
        }

        private async void StartAsync()
        {
            try
            {
                DontDestroyOnLoad(gameObject);
                EventSystem.Add(DLLType.Model, typeof(Init).Assembly);
            
                ResourceManager.Init();
                TcbManager.Init("hello-cloudbase-8gvdnkxi0dd7a257", 3000);
                //await TcbManager.Login("HusKka");
                
                await HotfixManager.LoadHotFixAssembly();

                HotfixManager.GotoHotfix();
                
                // EventSystem.Run(EventIdType.TestHotfixSubscribMonoEvent, "订阅成功！");
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
        
        private void Update()
        {
            HotfixManager.Update?.Invoke();
            EventSystem.Update();
        }

        private void LateUpdate()
        {
            HotfixManager.LateUpdate?.Invoke();
            EventSystem.LateUpdate();
        }

        private void OnApplicationQuit()
        {
            HotfixManager.OnApplicationQuit?.Invoke();
        }
    }
}
