﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using UnityEngine;

#if ILRuntime
using System.IO;
using System.Threading;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Generated;
#endif

namespace Model
{
    public static class HotfixManager
    {
#if ILRuntime
        private static ILRuntime.Runtime.Enviorment.AppDomain appDomain;
        private static MemoryStream dllStream;
        private static MemoryStream pdbStream;
#else
		private static Assembly assembly;
#endif

        private static IStaticMethod start;
        private static List<Type> hotfixTypes;
        
        public static Action Update;
        public static Action LateUpdate;
        public static Action OnApplicationQuit;
        
        public static List<Type> GetHotfixTypes()
        {
            return hotfixTypes;
        }
        
        public static void GotoHotfix()
        {
            start.Run();
        }
        
        public static async Task LoadHotFixAssembly()
        {
            var dllText = await ResourceManager.LoadAssetAsync<TextAsset>("Hotfix.dll.bytes");
            var pdbText = await ResourceManager.LoadAssetAsync<TextAsset>("Hotfix.pdb.bytes");
            
#if ILRuntime
            Log.Info($"当前使用的是ILRuntime模式");
            appDomain = new ILRuntime.Runtime.Enviorment.AppDomain();
            dllStream = new MemoryStream(dllText.bytes);
            pdbStream = new MemoryStream(pdbText.bytes);
            try
            {
                appDomain.LoadAssembly(dllStream, pdbStream, new ILRuntime.Mono.Cecil.Pdb.PdbReaderProvider());
            }
            catch
            {
                Debug.LogError("加载热更DLL失败!");
            }

            start = new ILStaticMethod(appDomain, "Hotfix.Init", "Start", 0);
            hotfixTypes = appDomain.LoadedTypes.Values.Select(x => x.ReflectionType).ToList();

            InitILRuntime();
#else
            Log.Info($"当前使用的是Mono模式");

			assembly = Assembly.Load(dllText.bytes, pdbText.bytes);

			Type hotfixInit = assembly.GetType("Hotfix.Init");
			start = new MonoStaticMethod(hotfixInit, "Start");
			
			hotfixTypes = assembly.GetTypes().ToList();
#endif
            
            ResourceManager.ReleaseAsset(dllText);
            ResourceManager.ReleaseAsset(pdbText);
        }
        
#if ILRuntime
        private static void InitILRuntime()
        {
#if DEBUG && (UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE)
            //由于Unity的Profiler接口只允许在主线程使用，为了避免出异常，需要告诉ILRuntime主线程的线程ID才能正确将函数运行耗时报告给Profiler
            appDomain.UnityMainThreadID = Thread.CurrentThread.ManagedThreadId;
#endif
            
            // 注册重定向函数
            // nothing yet
            
            // 注册委托
            appDomain.DelegateManager.RegisterMethodDelegate<List<object>>();
            appDomain.DelegateManager.RegisterMethodDelegate<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>();
            appDomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction>((act) =>
            {
                return new UnityEngine.Events.UnityAction(() =>
                {
                    ((Action)act)();
                });
            });
            appDomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.TweenCallback>((act) =>
            {
                return new DG.Tweening.TweenCallback(() =>
                {
                    ((Action)act)();
                });
            });


            
            CLRBindings.Initialize(appDomain);
            
            // 注册适配器
            RegisterAdaptor(appDomain);
            
            LitJson.JsonMapper.RegisterILRuntimeCLRRedirection(appDomain);
        }

        public static void RegisterAdaptor(ILRuntime.Runtime.Enviorment.AppDomain domain)
        {
            // 注册适配器
            Assembly assembly = typeof(Init).Assembly;
            foreach (Type type in assembly.GetTypes())
            {
                object[] attrs = type.GetCustomAttributes(typeof(ILAdapterAttribute), false);
                if (attrs.Length == 0)
                {
                    continue;
                }
                object obj = Activator.CreateInstance(type);
                CrossBindingAdaptor adaptor = obj as CrossBindingAdaptor;
                if (adaptor == null)
                {
                    continue;
                }
                domain.RegisterCrossBindingAdaptor(adaptor);
            }
        }
#endif
    }
}
