﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Model
{

    public enum SceneType
    {
        LoginScene,
        MainScene,
        BattleScene,
    }

    public class CustomLoading
    {
        public Action loadingStartAction; //开始loading
        public Func<bool> loadingCoverFunc; //检测loading界面是否覆盖整个界面（看不到后面的东西）
        public Action loadingFinishAction; //完成loading
        public Func<bool> loadingDispelFunc; //检测loading界面是否完全消失


        public CustomLoading(Action loadingStart, Func<bool> loadingCoverd, Action loadingFinish, Func<bool> loadingDispel)
        {
            loadingStartAction = loadingStart;
            loadingCoverFunc = loadingCoverd;
            loadingFinishAction = loadingFinish;
            loadingDispelFunc = loadingDispel;
        }
    }

    public class LoadingManager : MonoBehaviour
    {

        private static LoadingManager s_instance;

        public static LoadingManager Instance => s_instance;

        public static SceneType CurrentScene
        {
            get
            {
                string sceneName = SceneManager.GetActiveScene().name;
                if (sceneName.Contains(Define.InitScene) || sceneName == Define.EmptyScene)
                    return SceneType.LoginScene;
                if (sceneName == Define.MainScene)
                    return SceneType.MainScene;
                return SceneType.BattleScene;
            }
        }

        public CanvasGroup panel;
        public RawImage loadingTexture;
        public GameObject logo;
        
        private void DefaultLoadingStart()
        {
            EventSystem.Run(EventIdType.LoadingStart);
            panel.DOFade(1f, 1f).SetEase(Ease.InSine);
        }

        private bool DefaultLoadingCover()
        {
            return Math.Abs(panel.alpha - 1f) < 0.01f;
        }

        private void DefaultLoadingFinish()
        {
            panel.DOFade(0f, 1f).SetEase(Ease.OutSine);
        }

        private bool DefaultLoadingDispel()
        {
            return Math.Abs(panel.alpha - 0f) < 0.01f;
        }


        public GameObject invisibleMask;

        public WaitServerInputBreak waitServerBreak;

        private void Awake()
        {
            s_instance = this;
        }

        public void StartLoadEmptyScene(Action a, CustomLoading cl = null)
        {
            StartLoadSceneByName(Define.EmptyScene, a, cl);
        }

        public void StartLoadMainScene(Action a, CustomLoading cl = null)
        {
            StartLoadSceneByName(Define.MainScene, a, cl);
        }

        public void StartLoadBattleSceneByName(Action a, CustomLoading cl)
        {
            StartLoadSceneByName(Define.BattleScene, a, cl);
        }

        private bool isLoadingScene;
        public void StartLoadSceneByName(string sceneName, Action a, CustomLoading cl)
        {
            if (isLoadingScene)
            {
                Log.Error("场景正在加载中，请勿连续加载！");
                return;
            }
            
            StartCoroutine(LoadSceneAsync(sceneName, a, cl));
        }

        private IEnumerator LoadSceneAsync(string sceneName, Action finish, CustomLoading cl)
        {
            isLoadingScene = true;

            (cl?.loadingStartAction ?? DefaultLoadingStart).Invoke();

            while (true)
            {
                bool covered = (cl?.loadingCoverFunc ?? DefaultLoadingCover).Invoke();
                if (covered) break;

                yield return new WaitForSeconds(0.1f);
            }

            var op = ResourceManager.LoadSceneAsync(sceneName);
            yield return op;

            Resources.UnloadUnusedAssets();

            finish?.Invoke();

            //前面把该做的都做了，在取消遮挡之前等一下
            yield return 0;

            (cl?.loadingFinishAction ?? DefaultLoadingFinish).Invoke();

            while (true)
            {
                bool dispelled = (cl?.loadingDispelFunc ?? DefaultLoadingDispel).Invoke();
                if (dispelled) break;

                yield return new WaitForSeconds(0.1f);
            }

            isLoadingScene = false;
        }


        //--------------下面是一个界面切换的遮挡面板的一些方法-------------------
        private int biCount;
        public void SetBreakInput(bool on)
        {
            biCount += on ? 1 : -1;
            invisibleMask.SetActive(biCount > 0);
        }

        //--------------下面是一个网络等待的遮挡面板的一些方法-------------------
        public void SetWaitServerBreak(int protocal)
        {
            waitServerBreak.SetBreak(protocal);
        }

        public void CancelWaitServerBreak(int protocal)
        {
            waitServerBreak.CancelBreak(protocal);
        }

        public bool IsInputBreak()
        {
            return isLoadingScene || biCount > 0 || waitServerBreak.IsBreak();
        }

    }
}
