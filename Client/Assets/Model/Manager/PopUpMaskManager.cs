﻿using System.Threading.Tasks;
using UnityEngine;

namespace Model
{
    public static class PopUpMaskManager
    {
        private static GameObject s_gameObject;
        private static Transform s_transform;

        private static bool s_inited;

        public static async Task Init()
        {
            s_gameObject = await UIFactory.Create(null, Define.PopUpMaskPanel);
            s_transform = s_gameObject.transform;
            
            s_gameObject.SetActive(false);
            s_inited = true;
        }

        public static void SetDepth(Transform pTransform)
        {
            if (!s_inited || pTransform == null)
            {
                s_gameObject.SetActive(false);
                return;
            }

            if (s_transform.parent == pTransform.parent)
            {
                var tIndex = s_transform.GetSiblingIndex();
                var pIndex = pTransform.GetSiblingIndex();
                s_transform.SetSiblingIndex(pIndex - (tIndex < pIndex ? 1 : 0));
            }
            else
            {
                s_transform.parent = pTransform.parent;
                s_transform.SetSiblingIndex(pTransform.GetSiblingIndex());
            }
            
            if (!s_gameObject.activeSelf) s_gameObject.SetActive(true);
        }
    }
}