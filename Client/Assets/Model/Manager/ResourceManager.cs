﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.U2D;
using UnityEngine.Video;

namespace Model
{
    public static class ResourceManager
    {
        public static Action<AsyncOperationHandle> handleAction;

        public static AsyncOperationHandle LoadSceneAsync(object key)
        {
            return Addressables.LoadSceneAsync(key);
        }

        public static T LoadAsset<T>(object key)
        {
            var op = Addressables.LoadAssetAsync<T>(key);
            handleAction?.Invoke(op);

            return op.WaitForCompletion();
        }
        
        public static async Task<T> LoadAssetAsync<T>(object key)
        {
            var op = Addressables.LoadAssetAsync<T>(key);
            handleAction?.Invoke(op);
            
            await op.Task;

            if (op.Status == AsyncOperationStatus.Failed)
            {
                Debug.LogError($"LoadAsset failed! key:{key}");
            }
            
            return op.Result;
        }

        public static void ReleaseAsset(AsyncOperationHandle handle)
        {
            Addressables.Release(handle);
        }

        public static void ReleaseAsset(UnityEngine.Object asset)
        {
            Addressables.Release(asset);
        }

        public static void Init()
        {
            SpriteAtlasManager.atlasRequested += SpriteAtlasManagerOnAtlasRequested;
        }
        
        private static async void SpriteAtlasManagerOnAtlasRequested(string atlasName, Action<SpriteAtlas> callback)
        {
            var atlas = await LoadSpriteAtlasAsync(atlasName);
            callback(atlas);
        }
        
        public static async Task<SpriteAtlas> LoadSpriteAtlasAsync(string name)
        {
            return await LoadAssetAsync<SpriteAtlas>($"{Define.AtlasPath}{name}{Define.DotSpriteAtlas}");
        }
        
        public static async Task<Texture> LoadTextureAsync(string name)
        {
            return await LoadAssetAsync<Texture>($"{Define.TexturePath}{name}{Define.DotPng}");
        }
        
        public static async Task<AudioClip> LoadAudioClipAsync(string name, string ext)
        {
            return await LoadAssetAsync<AudioClip>($"{Define.SoundPath}{name}{ext}");
        }
        
        public static async Task<VideoClip> LoadVideoClipAsync(string name, string ext)
        {
            return await LoadAssetAsync<VideoClip>($"{Define.VideoPath}{name}{ext}");
        }

        public static async Task<GameObject> LoadUIPrefabAsync(string name)
        {
            var obj = await LoadAssetAsync<GameObject>($"{Define.UIPrefabPath}{name}{Define.DotPrefab}");
            return obj == null ? null : UnityEngine.Object.Instantiate(obj);
        }
        
        public static async Task<GameObject> LoadCharacterPrefabAsync(string name)
        {
            var obj = await LoadAssetAsync<GameObject>($"{Define.CharacterPrefabPath}{name}{Define.DotPrefab}");
            return obj == null ? null : UnityEngine.Object.Instantiate(obj);
        }
        
        public static async Task<GameObject> LoadParticlePrefabAsync(string name)
        {
            var obj = await LoadAssetAsync<GameObject>($"{Define.ParticlePrefabPath}{name}{Define.DotPrefab}");
            return obj == null ? null : UnityEngine.Object.Instantiate(obj);
        }
        
    }
}
