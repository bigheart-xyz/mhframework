﻿using System.Collections.Generic;
using System.Threading.Tasks;
using com.unity.cloudbase;
using UnityEngine;

public static class TcbManager
{
    private static CloudBaseApp app;

    public static void Init(string env, int timeout)
    {
        if (app != null)
        {
            Debug.LogError("TcbManager.Init failed! app has exist!");
            return;
        }

        app = CloudBaseApp.Init(env, timeout);
    }

    public static async Task<bool> Login(string customUserId)
    {
        var state = await app.Auth.GetAuthStateAsync();
        if (state != null)
        {
            await app.Auth.SignOutAsync();
        }

        await app.Auth.SignInAnonymouslyAsync();

        var response = await app.Function.CallFunctionAsync("RequestTicket", new Dictionary<string, string> {{"CustomUserId", customUserId}});
        if (!string.IsNullOrEmpty(response.Code))
        {
            Debug.LogError(response.Message);
            return false;
        }
        
        var ticket = response.Data["ticket"]?.ToString();
        state = await app.Auth.SignInWithTicketAsync(ticket);
        return state != null;
    }
}
