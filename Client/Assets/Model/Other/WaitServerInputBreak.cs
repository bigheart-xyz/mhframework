﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace Model
{
    public class WaitServerInputBreak : MonoBehaviour
    {
        public CanvasGroup mask;
        public Image icon;

        public float hidingWaitTime = 1f;

        public Dictionary<int, int> pDic = new Dictionary<int, int>();
        public int totalPNum;

        private float accTime;
        private float needTime = 0.12f;

        private Tweener tween;

        private void Update()
        {
            accTime += Time.deltaTime;
            if (accTime >= needTime)
            {
                accTime -= needTime;
                icon.transform.Rotate(0f, 0f, -45f);
            }
        }

        public void SetBreak(int protocal)
        {
            if (pDic.TryGetValue(protocal, out var num))
            {
                num++;
            }
            else num = 1;

            pDic[protocal] = num;
            totalPNum++;

            SetBreak();
        }

        public void CancelBreak(int protocal)
        {
            if (pDic.TryGetValue(protocal, out var num))
            {
                num--;
                if (num == 0) pDic.Remove(protocal);
                else pDic[protocal] = num;

                totalPNum--;
                if (totalPNum == 0) CancelBreak();
            }
        }

        private void SetBreak()
        {
            tween?.Kill();

            gameObject.SetActive(true);
            mask.alpha = 0.01f;
            tween = mask.DOFade(1f, 0.3f).SetEase(Ease.OutSine).SetDelay(hidingWaitTime);
        }

        private void CancelBreak()
        {
            tween?.Kill();
            if (mask.alpha <= 0.01f) gameObject.SetActive(false);
            else tween = mask.DOFade(0.01f, 0.2f).SetEase(Ease.InSine).OnComplete(() => gameObject.SetActive(false));
        }

        public bool IsBreak()
        {
            return totalPNum > 0;
        }

    }
}
