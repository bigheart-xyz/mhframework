﻿using System;

namespace Model
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public class UIAttribute: BaseAttribute
	{
		public string Name { get; }

		public UIAttribute(string name)
		{
			this.Name = name;
		}
	}
}