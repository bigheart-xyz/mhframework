using Coffee.UIEffects;
using UnityEngine;
using UnityEngine.UI;

namespace Model
{
    public static class UIExtension
    {
        public static void SetGray(this Graphic graphic, bool gray)
        {
            if (!graphic) return;
            
            var uiEffect = graphic.GetComponent<UIEffect>();
            if (!uiEffect)
            {
                uiEffect = graphic.gameObject.AddComponent<UIEffect>();
            }

            uiEffect.effectMode = EffectMode.Grayscale;
            uiEffect.effectFactor = gray ? 1 : 0;
        }

        public static void SetGrayHierarchy(this GameObject gameObject, bool gray)
        {
            if (!gameObject) return;

            var graphics = gameObject.GetComponentsInChildren<Graphic>();
            foreach (var graphic in graphics)
            {
                SetGray(graphic, gray);
            }
        }
    }
}
