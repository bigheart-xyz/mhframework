﻿using System.Threading.Tasks;
using UnityEngine;

namespace Model
{
    public static class UIFactory
    {
        private static RectTransform s_parent;
        private static RectTransform Parent
        {
            get
            {
                if (s_parent == null)
                {
                    s_parent = GameObject.Find("GUI/Panels").transform as RectTransform;
                }
                return s_parent;
            }
        }
        
        private static void SetParent(RectTransform t, RectTransform parent)
        {
            t.SetParent(parent);
            t.pivot = parent.pivot;
            t.anchorMin = parent.anchorMin;
            t.anchorMax = parent.anchorMax;
            t.sizeDelta = parent.sizeDelta;
            
            t.localPosition = Vector3.zero;
            t.localScale = Vector3.one;
        }

        public static async Task<GameObject> Create(RectTransform parent, string name)
        {
            LoadingManager.Instance.SetBreakInput(true);

            GameObject go;
            if (parent == null) parent = Parent;
            var t = parent.Find(name);
            if (t == null)
            {
                go = await ResourceManager.LoadUIPrefabAsync(name);
                if (go == null) return null;

                go.SetActive(true);
                go.name = name;
                t = go.transform;
                SetParent(t as RectTransform, parent);

                go.GetComponent<CanvasGroup>().alpha = 0;
            }
            else
            {
                go = t.gameObject;
            }
            
            LoadingManager.Instance.SetBreakInput(false);

            return go;
        }
        
    }
}
