set WORKSPACE=..
set GEN_CLIENT=%WORKSPACE%\Tools\Luban.Client\Luban.Client.exe
set CONF_ROOT=%WORKSPACE%\Configs
set DEFINE_FILE=%CONF_ROOT%\Defines\__root__.xml

%GEN_CLIENT% -h %LUBAN_SERVER_IP% -j cfg --^
 -d %DEFINE_FILE%^
 --input_data_dir %CONF_ROOT%\Datas ^
 --output_code_dir %WORKSPACE%\..\Client\Assets\Hotfix\Config\Generate ^
 --output_data_dir %WORKSPACE%\..\Client\Assets\Res\Config ^
 --gen_types code_cs_bin,data_bin ^
 --use_unity_vector ^
 -s client ^
--export_test_data

pause