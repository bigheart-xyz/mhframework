'use strict';

const cloudbase = require("@cloudbase/node-sdk");

exports.main = async (event, context) => {
    // 1. 初始化 SDK
    const app = cloudbase.init({
        env: "hello-cloudbase-8gvdnkxi0dd7a257",
        // 传入自定义登录私钥
        credentials: require("tcb_custom_login_key.json")
    });
    // 2. 开发者自定义的用户唯一身份标识
    const customUserId = event["CustomUserId"];
    // 3. 创建ticket
    const ticket = app.auth().createTicket(customUserId);
    // 4. 将ticket返回至客户端
    console.log("ticket:" + ticket);
    return { "ticket": ticket };
};
